import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const String poppins = 'Poppins';
const String roboto = 'Roboto';

class JelajahinTheme {
  static const Color primaryColor = Color(0xFF2B936A);
  static const Color secondaryColor = Color(0xFFC2FAE4);
  static const Color whiteColor = Color(0xFFFFFFFF);
  static const Color tertiaryColorLighter = Color(0xFF8D8D8D);
  static const Color tertiaryColor = Color(0xFF515151);
  static const Color tertiaryDarkerColor = Color(0xFF3D3D3D);
  static const Color tertiaryBlackColor = Color(0xFF1F1F1F);

  static TextStyle get title => GoogleFonts.getFont(
        poppins,
        color: tertiaryDarkerColor,
        fontWeight: FontWeight.w600,
        fontSize: 24,
      );

  static TextStyle get subtitle => GoogleFonts.getFont(
        poppins,
        color: tertiaryColorLighter,
        fontWeight: FontWeight.w500,
        fontSize: 18,
      );

  static TextStyle get bodyText => GoogleFonts.getFont(
        poppins,
        color: tertiaryDarkerColor,
        fontWeight: FontWeight.normal,
        fontSize: 14,
      );
}
