import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

extension TextStyleHelper on TextStyle {
  TextStyle override(
      {String? fontFamily,
        Color? color,
        double? fontSize,
        FontWeight? fontWeight,
        FontStyle? fontStyle}) =>
      GoogleFonts.getFont(
        fontFamily!,
        color: color ?? this.color,
        fontSize: fontSize ?? this.fontSize,
        fontWeight: fontWeight ?? this.fontWeight,
        fontStyle: fontStyle ?? this.fontStyle,
      );
}