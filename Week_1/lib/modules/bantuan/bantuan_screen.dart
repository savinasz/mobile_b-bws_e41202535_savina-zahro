import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jelajahin_keeper/constant/style_const.dart';

class BantuanScreen extends StatefulWidget{
  const BantuanScreen({
    Key? key
  }) : super (key: key);

  @override
  _BantuanScreenState createState() => _BantuanScreenState();
}

class _BantuanScreenState extends State<BantuanScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: JelajahinTheme.secondaryColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.black,),
          onPressed: (){},
        ),
        title: Center(
          child : const Text("Pusat Bantuan", style: TextStyle(color: Colors.black)))
      ),
      body: SafeArea(
        child: Column(
          children: [
            _buildBantuanHeader(),
            _buildBodyBantuan()],
        ),
      ),
    );
  }

  Widget _buildBantuanHeader(){
    return SizedBox(
      height: 225.0,
      child: Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 200.0,
            child: ColoredBox(color: JelajahinTheme.secondaryColor),
          ),
          _buildTampilanHeader()
        ],
      ),
    );
  }

  Widget _buildSearchBantuan(){
    return SizedBox();
  }
  Widget _buildTampilanHeader(){
    return Container(
      padding: EdgeInsets.only(left: 25, bottom: 40),
      child: Row(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Hai, Savina", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
            Text("Ada yang bisa dibantu?",style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
            Text("Kamu bisa cari dan baca topik yang"),
            Text("ingin ditanyakan!")
          ],
        ),
        Image.asset('assets/images/im_bantuan.png', width: 139, height: 108,)
      ],
    ),
    );
  }
  Widget _buildBodyBantuan(){
    return Container(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Pilih Topik Sesuai Kendalamu',
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
          _buildCategoryOfQuestion(),
          Text('Hubungi Kami',
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
          _buildContact()
      ],
      ),
    );
  }
  Widget _buildCategoryOfQuestion(){
    return GestureDetector(
        child: Container(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                  child: Row(
                    children: [Text("Pembayaran",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16))]
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                  child: Row(
                      children: [ Text("Akun",
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                  child: Row(
                    children: [ Text("Informasi Umum",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                  child: Row(
                    children: [ Text("Pencarian",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Row(
                    children: [ Text("Mendaftarkan Event",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),],
                  ),
                ),
              ],
            )
        ),
    );
  }

  Widget _buildPembayaran(){
    return Container(
    );
  }

  Widget _buildContact(){
    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20.0, left: 10.0),
            child: GestureDetector(
              onTap: () {},
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Icon (Icons.email_outlined, size: 24.0,),),
                  Text('Ajukan Pertanyaan Kamu', style: TextStyle(fontSize: 16))
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0, left: 10.0),
            child: GestureDetector(
              onTap: () {},
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Icon (Icons.attach_email_outlined, size: 24.0,),),
                  Text('Email', style: TextStyle(fontSize: 16))
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0, left: 10.0),
            child: GestureDetector(
              onTap: () {},
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Icon (Icons.phone, size: 24.0,),),
                  Text('Telepon', style: TextStyle(fontSize: 16))
                ],
              ),
            ),
          ),
        ],
      )
    );
  }

}