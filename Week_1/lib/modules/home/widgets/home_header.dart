import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jelajahin_keeper/constant/style_const.dart';

const imageUrl =
    "https://spesialis1.orthopaedi.fk.unair.ac.id/wp-content/uploads/2021/02/depositphotos_39258143-stock-illustration-businessman-avatar-profile-picture.jpg";

class BuildHomeHeader extends StatefulWidget {
  const BuildHomeHeader({Key? key}) : super(key: key);

  @override
  _BuildHomeHeaderState createState() => _BuildHomeHeaderState();
}

class _BuildHomeHeaderState extends State<BuildHomeHeader> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [_buildKeepersEmerald(0), _buildNotificationAndProfile()],
      ),
    );
  }

  Widget _buildNotificationAndProfile() {
    return Padding(
      padding: const EdgeInsets.only(right: 16),
      child: Row(
        children: [
          IconButton(
            iconSize: 32,
              onPressed: () {},
              icon: const Icon(
                Icons.notifications_active,
                color: JelajahinTheme.whiteColor,
              )),
          const CircleAvatar(
            backgroundImage: NetworkImage(imageUrl),
          )
        ],
      ),
    );
  }

  Widget _buildKeepersEmerald(int currentEmerald) {
    return Container(
      margin: const EdgeInsets.only(left: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(26.0),
        color: JelajahinTheme.secondaryColor,
      ),
      width: 180,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Row(
          children: [
            const SizedBox(
              width: 12,
            ),
            SvgPicture.asset('assets/images/ic_emerald.svg'),
            const SizedBox(
              width: 12,
            ),
            Container(
              color: JelajahinTheme.secondaryColor,
              child: Text(
                "$currentEmerald",
                style: const TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: JelajahinTheme.primaryColor),
              ),
            ),
            const Spacer(),
            CircleAvatar(
              backgroundColor: JelajahinTheme.primaryColor,
              child: Center(
                child: IconButton(
                  color: Colors.white,
                  splashColor: Colors.transparent,
                  enableFeedback: false,
                  onPressed: () => {},
                  icon: const Icon(Icons.add),
                ),
              ),
            ),
            const SizedBox(
              width: 6,
            ),
          ],
        ),
      ),
    );
  }
}
