import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jelajahin_keeper/constant/style_const.dart';
import 'package:jelajahin_keeper/modules/home/widgets/home_header.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [_buildPointSection(), _buildHomeMenus()],
        ),
      ),
    );
  }

  Widget _buildHomeMenus() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: (){},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset('assets/icon/ic_promotion.svg'),
              const SizedBox(height: 6,),
              const Text('Promosi', style: TextStyle(fontWeight: FontWeight.w400),)
            ],
          ),
        ),
        GestureDetector(
          onTap: (){},
          child: Column(
            children: [
              SvgPicture.asset('assets/icon/ic_create_event.svg'),
              const SizedBox(height: 6,),
              const Text('Acara', style: TextStyle(fontWeight: FontWeight.w400),)
            ],
          ),
        ),
        GestureDetector(
          onTap: (){},
          child: Column(
            children: [
              SvgPicture.asset('assets/icon/ic_help.svg'),
              const SizedBox(height: 6,),
              const Text('Bantuan', style: TextStyle(fontWeight: FontWeight.w400),)
            ],
          ),
        ),

      ],
    );
  }

  Widget _buildPointSection() {
    return SizedBox(
      height: 255.0,
      child: Stack(
        children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 200.0,
              child: Image.asset(
                "assets/images/bg_home.jpg",
                fit: BoxFit.fitWidth,
              )),
          const Positioned(
              top: 0.0, left: 0.0, right: 0.0, child: BuildHomeHeader()),
          Positioned(
            top: 165.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              padding: const EdgeInsets.symmetric(vertical: 12),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(22.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 6.0,
                    ),
                  ],
                  color: Colors.white),
              child: IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _buildButtonProperty(),
                    const VerticalDivider(
                        color: JelajahinTheme.tertiaryDarkerColor,
                        thickness: 1),
                    _buildButtonReport()
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtonReport() {
    return GestureDetector(
      onTap: () {},
      child: Row(
        children: [
          SizedBox(
              height: 28,
              width: 28,
              child: SvgPicture.asset('assets/icon/ic_keeper_report.svg')),
          const SizedBox(
            width: 12,
          ),
          const Text(
            "Laporan",
            style: TextStyle(
                fontFamily: poppins,
                color: JelajahinTheme.tertiaryDarkerColor,
                fontWeight: FontWeight.w500,
                fontSize: 16),
          )
        ],
      ),
    );
  }

  Widget _buildButtonProperty() {
    return GestureDetector(
      onTap: () {},
      child: Row(
        children: [
          SizedBox(
              height: 28,
              width: 28,
              child: SvgPicture.asset('assets/icon/ic_property.svg')),
          const SizedBox(
            width: 12,
          ),
          const Text(
            "Properti mu",
            style: TextStyle(
                fontFamily: poppins,
                color: JelajahinTheme.tertiaryDarkerColor,
                fontWeight: FontWeight.w500,
                fontSize: 16),
          )
        ],
      ),
    );
  }
}
