import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jelajahin_keeper/constant/style_const.dart';

class ProfileScreen extends StatefulWidget{
  const ProfileScreen({
    Key? key
}) : super (key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [_buildProfileBgHeader(), _buildProfileMenu()],
        ),
      ),
    );
  }

  Widget _buildProfileBgHeader(){
    return SizedBox(
      height: 255.0,
      child: Stack(
        children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 250.0,
              child: Container(
                decoration: BoxDecoration(
                    color: JelajahinTheme.primaryColor,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100.0),
                        bottomRight: Radius.circular(100.0)),
                ),
              ),
          ),
          _buildProfileHeader(),
        ],
      ),
    );
  }

  Widget _buildProfileHeader(){
    return Column(
      children: [
        _buildProfile(),
        _buildData(),
         _buildMembers()
      ],
    );
  }
  Widget _buildProfile(){
    return Row(
      children: <Widget>[
        _buildImageProfile(),
        _buildNameAndRating()],
    );
  }

  Widget _buildImageProfile(){
    return Container(
      padding: EdgeInsets.only(left: 80.0, top: 30.0, right: 15.0),
        child: CircleAvatar(
            backgroundImage: NetworkImage
              ("https://www.iconspng.com/images/young-user-icon.jpg"))
    );
  }

  Widget _buildNameAndRating(){
    return Container(
      padding: EdgeInsets.only(top: 50.0),
      child: Column(
      children: <Widget>[
        const Text(
          "Savina Zahro",
          style: TextStyle(
              color: JelajahinTheme.whiteColor,
              fontFamily: poppins,
              fontWeight: FontWeight.w400,
              fontSize: 16),
        ),
        Container(
            margin: EdgeInsets.only(top: 5.0),
            padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),
            height: 30,
            decoration: BoxDecoration(
              color: Colors.white12,
              borderRadius: BorderRadius.circular(15.0)),
          child: Row(
          children: [
            Image.asset('assets/icon/ic_stars.png'),
            Text(" 4.8",
              style: TextStyle(
                fontSize: 17,
                color: JelajahinTheme.whiteColor
              ) ,),
          ],
          )
        )
      ]
    )
    );
  }

  Widget _buildData(){
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildAkomodation(),
        _buildFollowers(),
        _buildPerforma()
      ],
    )
    );
  }

  Widget _buildAkomodation(){
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20),
      child: Column(
      children: [
        Text("12", style: TextStyle(fontSize: 20, fontFamily: poppins, color: JelajahinTheme.whiteColor),
        ),
        const Text("Hotel Facility", style: TextStyle(color: Colors.white, fontSize: 12),
        )
      ],
    ),
    );
  }

  Widget _buildFollowers(){
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20),
      child: Column(
        children: [
          Text("12", style: TextStyle(fontSize: 20, fontFamily: poppins, color: JelajahinTheme.whiteColor),
          ),
          const Text("Followers", style: TextStyle(color: Colors.white, fontSize: 12),
          )
        ],
      ),
    );
  }

  Widget _buildPerforma(){
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20),
      child: Column(
        children: [
          Text("12", style: TextStyle(fontSize: 20, fontFamily: poppins, color: JelajahinTheme.whiteColor),
          ),
          const Text("Performa", style: TextStyle(color: Colors.white, fontSize: 12),
          )
        ],
      ),
    );
  }

  Widget _buildMembers(){
    return Container(
      margin: EdgeInsets.only(top: 16.0),
      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),
      height: 85,
      width: 300,
      decoration: BoxDecoration(
          color: Colors.black12,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20.0), topLeft: Radius.circular(20.0),
              bottomLeft: Radius.circular(30.0), bottomRight: Radius.circular(30.0))),
    );
  }

  Widget _buildProfileMenu() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Card(child: _buildSettingButton()),
          Card(child: _buildSyaratButton()),
          Card(child: _buildKebijakanButton()),
          Card(child: _buildAboutJelajahinButton()),
          Card(child: _buildLogoutButton())
        ],
    );
  }

  Widget _buildSettingButton(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                decoration: BoxDecoration(
                  color: JelajahinTheme.secondaryColor,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Image.asset('assets/icon/ic_setting.png'),
              ),
              const Text('Pengaturan', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16))
            ],
          ),
        ),
      ],

    );
  }

  Widget _buildSyaratButton(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                decoration: BoxDecoration(
                  color: JelajahinTheme.secondaryColor,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Image.asset('assets/icon/ic_syarat_ketentuan.png'),
              ),
              const Text('Syarat & Ketentuan', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16))
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildKebijakanButton(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                decoration: BoxDecoration(
                  color: JelajahinTheme.secondaryColor,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Image.asset('assets/icon/ic_kebijakan_privasi.png'),
              ),
              const Text('Kebijakan Privasi', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16))
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildAboutJelajahinButton() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                decoration: BoxDecoration(
                  color: JelajahinTheme.secondaryColor,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Image.asset('assets/icon/ic_tentang_jelajahin.png'),
              ),
              const Text('Tentang Jelajahin Bussiness', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16))
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildLogoutButton(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
                decoration: BoxDecoration(
                  color: JelajahinTheme.secondaryColor,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Image.asset('assets/icon/ic_logout.png'),
              ),
              const Text('Logout', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16))
            ],
          ),
        ),
      ],
    );
  }
}
