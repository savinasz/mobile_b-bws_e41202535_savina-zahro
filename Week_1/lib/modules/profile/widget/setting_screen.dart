import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jelajahin_keeper/constant/style_const.dart';

class SettingScreen extends StatefulWidget{
  const SettingScreen({
    Key? key
  }) : super (key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: JelajahinTheme.primaryColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: (){},),
        title: const Text("Pengaturan"),
      ),
      body: SafeArea(
        child: Column(
          children: [_buildSearch(), _buildAkun(), _buildNotifikasi(), _buildBahasa(), _buildKeamanan(), _buildTema()],
        ),
      ),
    );
  }

  Widget _buildNotifikasi(){
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      child:
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 10.0),
                child: Icon (Icons.notifications_none_rounded, size: 32.0,),
              ),
              Text('Notifikasi', style: TextStyle(fontSize: 20))
            ],
          ),
        ),
    );
  }

  Widget _buildBahasa(){
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      child:
      GestureDetector(
        onTap: () {},
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10.0),
              child: Icon (Icons.language_outlined, size: 32.0),
            ),
            Text('Bahasa', style: TextStyle(fontSize: 20)),
          ],
        ),
      ),
    );
  }

  Widget _buildAkun(){
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      child:
      GestureDetector(
        onTap: () {},
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10.0),
              child: Icon (Icons.account_circle_outlined, size: 32.0),
            ),
            Text('Akun', style: TextStyle(fontSize: 20)),
          ],
        ),
      ),
    );
  }

  Widget _buildTema(){
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      child:
      GestureDetector(
        onTap: () {},
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10.0),
              child: Icon (Icons.format_paint_outlined, size: 32.0),
            ),
            Text('Tema', style: TextStyle(fontSize: 20)),
          ],
        ),
      ),
    );
  }

  Widget _buildKeamanan(){
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      child:
      GestureDetector(
        onTap: () {},
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10.0),
              child: Icon (Icons.security, size: 32.0),
            ),
            Text('Keamanan', style: TextStyle(fontSize: 20)),
          ],
        ),
      ),
    );
  }

  Widget _buildSearch(){
    return Container();
  }
}